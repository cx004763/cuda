#include<stdio.h>
#include<stdlib.h>
#include<math.h>

__global__ void add(int *a, int *b, int *c) {
	c[blockIdx.x] = a[blockIdx.x] + b[blockIdx.x];
}

void random_ints(int* data, int count)
{
	for (int i =0 ;i < count;i++)
		data[i] = rand()%1000;
}

int main(void) {

	int n = 10;		//size of vectors
	int *a, *b, *c;		//host copies of a, b, c
	int *d_a, *d_b, *d_c;	//device copies of a, b, c
	int size = n  * sizeof(int);

	// Allocate Space for Local ABC Arrays

	a = (int*) calloc(n, sizeof(int));
	b = (int*) calloc(n, sizeof(int));
	c = (int*) calloc(n, sizeof(int));

	//Allocate space for device copies of a, b, c
	printf("\nAllocating Memory");
	cudaMalloc((void **)&d_a, size);
	cudaMalloc((void **)&d_b, size);
	cudaMalloc((void **)&d_c, size);
	// Populate the Arrays with 512 random ints
	random_ints(a, n);
	random_ints(b, n);
	random_ints(c, n);

	printf("\n\nRandom Numbers Generated");
	for(int i=0 ;i < n;i++)
		printf("\n[%2d] %3d %3d %3d",i, a[i], b[i], c[i]);

	//Copy inputs to device
	cudaMemcpy(d_a, &a, size, cudaMemcpyHostToDevice);
	cudaMemcpy(d_b, &b, size, cudaMemcpyHostToDevice);
	cudaMemcpy(d_c, &c, size, cudaMemcpyHostToDevice);
	printf("\n\nValues Copied");
	//for(int i=0 ;i<n;i++)
	//	printf("\n[%2d] %3d %3d %3d",i,d_a[i],d_b[i],d_c[i]);

	//Launch add() kernel on GPU
	add<<<1,1>>>(d_a, d_b, d_c);

	printf("\n\nValues Edited");
	//for(int i =0 ;i < n;i++)
	//	printf("\n[%2d] %3d %3d %3d",i,d_a[i],d_b[i],d_c[i]);

	//Copy result back to host
	printf("\npray4nosegfault");
	cudaMemcpy(&c, d_c, size, cudaMemcpyDeviceToHost);
	printf("\nnosegfault");
	getchar();
	printf("\n\nCopied Back");
	for(int i=0 ;i < n;i++)
		printf("\n[%2d] %3d %3d %3d",i, a[i], b[i], c[i]);

	//Cleanup
	cudaFree(d_a); cudaFree(d_b); cudaFree(d_c);
	printf("\n\nCuda Memory Freed\n\n\n");
	return 0;
}
